function stripped (str) {
  return str.toLowerCase().replace(/ /g,'-').replace(/,/g,'-');
}

function hrefWithoutHash () {
  return window.location.pathname;
}

window.addEventListener('hashchange', function() {
  console.log(hrefWithoutHash());
  console.log(decodeURI(location.hash.substr(1)));
  console.log(hrefWithoutHash() + '#' + decodeURI(location.hash.substr(1)));
  $("#ppi_2017").html("");
  $("#ppi_2025").html("");
  $("#csv-display").html("");
  $(".tooltip").remove();
  dataviz();
}, false);

console.log(hrefWithoutHash());
console.log(decodeURI(location.hash.substr(1)));
console.log(hrefWithoutHash() + '#' + decodeURI(location.hash.substr(1)));
dataviz();

function dataviz() {
  $.ajax({
    type: "GET",  
    url: "/data/ppi.csv",
    dataType: "text",       
    success: function(response)  
    {
      data = $.csv.toArrays(response);

      hashExists = false;

      for(i=1;i<data.length;i++) {
        if(
            location.hash != "" && 
            stripped(data[i][0]) == decodeURI(location.hash.substr(1))
        ) {
          hashExists = true;
          category_name = data[i][0];
          break;
        }
      }

      categories_2017 = [];
      categories_2025 = [];
      cat_cpt=0;
      cat_curr="";
      total_2017 = 0;
      total_2025 = 0;

      if(hashExists) {

        $(".article h1").html($(".article h1").html() + " > " + category_name + "<br /><a class=\"btn btn-default\" role=\"button\" href=\"" + hrefWithoutHash() + "\">revenir au général</a>");

        tData = [];
        tData[0] = data[0];
        for(i=1;i<data.length;i++) {

          if(stripped(data[i][0]) == decodeURI(location.hash.substr(1))) {

            tData[cat_cpt+1] = data[i];
            
            categories_2017[cat_cpt] = {};
            categories_2017[cat_cpt]['name'] = data[i][1];
            categories_2017[cat_cpt]['value'] = parseInt(data[i][2]);
            total_2017 += parseInt(data[i][2]);
            
            categories_2025[cat_cpt] = {};
            categories_2025[cat_cpt]['name'] = data[i][1];
            categories_2025[cat_cpt]['value'] = parseInt(data[i][3]);
            total_2025 += parseInt(data[i][3]);

            cat_cpt++;

          }

        }

        for(i=0;i<categories_2017.length;i++) {
          
          categories_2017[i]['pct'] = categories_2017[i]['value']*100/total_2017;
          categories_2025[i]['pct'] = categories_2025[i]['value']*100/total_2025;

        }

      } else {

        tData = data;
        for(i=1;i<data.length;i++) {

          if(categories_2017.length <= cat_cpt) {
            categories_2017[cat_cpt] = {};
            categories_2017[cat_cpt]['name'] = data[i][0];
            categories_2017[cat_cpt]['value'] = 0;
            
            categories_2025[cat_cpt] = {};
            categories_2025[cat_cpt]['name'] = data[i][0];
            categories_2025[cat_cpt]['value'] = 0;
          }
          
          categories_2017[cat_cpt]['value'] += parseInt(data[i][2]);
          total_2017 += parseInt(data[i][2]);
          categories_2025[cat_cpt]['value'] += parseInt(data[i][3]);
          total_2025 += parseInt(data[i][3]);

          if( cat_curr!=data[i][0] ) {
            if(cat_curr!="") {
              cat_cpt++;
            }
            cat_curr=data[i][0];
          }

        }

        for(i=0;i<categories_2017.length;i++) {
          
          categories_2017[i]['pct'] = categories_2017[i]['value']*100/total_2017;
          categories_2025[i]['pct'] = categories_2025[i]['value']*100/total_2025;

        }

      }

      colors = [];
      for(i=0;i<Object.keys(categories_2017).length;i++) {
          colors.push('#'+Math.floor(Math.random()*16777215).toString(16));
      }

      var colors = d3.scale.category20();
  
      generatePie(categories_2017, '#ppi_2017', colors, hashExists);
      generatePie(categories_2025, '#ppi_2025', colors, hashExists);
      generateHtmlTable(tData, colors);

    }   
  });
}