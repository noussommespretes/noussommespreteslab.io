//https://codepen.io/vlt5/pen/NqOvoG
function generatePie(data, div_id, colors) {

var margin = {top:0,left:0,right:0,bottom:0};

width = 350;
height = 300;
radius = Math.min(width-50,height-50)/2;

var arc = d3.svg.arc()  
    .outerRadius(radius - 50)
    .innerRadius(radius - 10);

var arcOver = d3.svg.arc()  
    .outerRadius(radius)
    .innerRadius(0);

var svg = d3.select(div_id).append("svg")
    .attr("height",height)
    .append("g")
    .attr("transform","translate("+width/2+","+height/2+")");

div = d3.select("body")
    .append("div") 
    .attr("class", "tooltip");

var pie = d3.layout.pie()
    .sort(null)
    .value(function(d){return d.value;});

var g = svg.selectAll(".arc")
    .data(pie(data))
    .enter()
    .append("g")
    .attr("class","arc")
    .on("mousemove",function(d){
        var mouseVal = d3.mouse(this);
        div.style("display","none");
        div
            .html("<h4>"+d.data.name+"</h4><ul><li>part: "+ Math.round(d.data.pct * 10) / 10 + "%</li><li>total catégorie: " + Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(d.data.value) + "</li></ul>")
            .style("left", (d3.event.pageX+12) + "px")
            .style("top", (d3.event.pageY-10) + "px")
            .style("opacity", 1)
            .style("display","block");
    })
    .on("mouseout",function(){div.html(" ").style("display","none");})
    .on("click",function(d){
        if(!hashExists) {
            window.location = hrefWithoutHash() + '#' + stripped(d.data.name);
        }
    });
            
g.append("path")
    .attr("d",arc)
    .style("fill",function(d){return colors(d.data.name);});

svg.selectAll("text").data(pie(data)).enter()
    .append("text")
    .attr("class","label1")
    .attr("transform", function(d) {
        var dist=radius+15;
        var winkel=(d.startAngle+d.endAngle)/2;
        var x=dist*Math.sin(winkel)-4;
        var y=-dist*Math.cos(winkel)-4;
		    		   
        return "translate(" + x + "," + y + ")";
    })
    .attr("dy", "0.35em")
    .attr("text-anchor", "middle")
    .text(function(d){
        return "";
    });
		    	    	 
}