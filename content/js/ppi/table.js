function generateHtmlTable(data, colors, div_id) {

    var html = '<table  class="table table-condensed table-hover table-striped">';
  
    if(typeof(data[0]) === 'undefined') {

        return null;

    } else {
    
        $.each(data, function( index, row ) {
    
            //bind header
            if(index == 0) {
                html += '<thead>';
                html += '<tr>';
                $.each(row, function( index, colData ) {
                    html += '<th>';
                    html += colData;
                    html += '</th>';
                });
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
            } else {
                if(hashExists) {
                    html += '<tr style="background-color: ' + colors(row[1]) + '">';
                } else {
                    html += '<tr style="background-color: ' + colors(row[0]) + '">';
                }
                $.each(row, function( index, colData ) {
                    html += '<td>';
                    if(index==0) {
                        html += "<a href=\"" + hrefWithoutHash() + "#" + stripped(colData) + "\">";
                    }
                    if(index==2 || index == 3) {
                        html += Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(colData);
                    } else {
                        html += colData;
                    }
                    if(index==0) {
                        html += "</a>";
                    }
                    html += '</td>';
                });
                html += '</tr>';
            }

        });

        html += '</tbody>';
        html += '</table>';
        
        $('#csv-display').append(html);

    }
}	
  